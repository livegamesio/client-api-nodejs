//Handler
var apiKey = 'testApiKey';
var apiSecret = 'testApiSecret';
var liveGamesHandler = require('../index')(apiKey, apiSecret).Handler;

//Webserver simulator
var request = require('supertest');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var testPort = 9191;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// helper method
var generateDummyNumber = function() {
	return parseInt(Math.random() * 987654321);
};

//Express middleware integration -->
app.use('/livegames/seamlesscallback', liveGamesHandler.createMiddleware({
	debug: true,
	//bindings
	bindings: {

		GetWallet: function(data, cb) {

			//your logic here ---->

			//This is test logic
			var userId = data.uid;

			var userCredit = (userId == "richie") ? 99999 : 0;
			var errorResponse = (userId == "wrongUser") ? "INVALID_USER" : null;

			var responseObject = {
				credit: userCredit
			};

			// <---- your logic here!

			cb(errorResponse, responseObject);
		},

		UpdateWallet: function(data, cb) {

			//your logic here ---->

			//This is test logic
			var userId = data.uid;

			var userCredit = (userId == "richie") ? 77777 : 0;
			var errorResponse = (userId == "wrongUser") ? "INVALID_USER" : null;


			var responseObject = {};

			if (!errorResponse) {
				var clientTransactionId = generateDummyNumber(); //it should be same id with your db record

				responseObject.credit = userCredit;
				responseObject.id = clientTransactionId;
				//....
			}

			// <---- your logic here!

			cb(errorResponse, responseObject);
		}

	}
}));


//------- Test Methods ------------------------------------

var makeTestRequest = function(action, reqData) {
	var testRequest = {
		lgAction: action,
		lgData: reqData,
		lgSec: liveGamesHandler.makeRequestSecret(reqData)
	};
	request(app)
		.post('/livegames/seamlesscallback')
		.send(testRequest)
		.expect('Content-Type', /json/)
		.expect(200)
		.end(function(err, res) {
			if (err) throw err;
			//console.log(`TEST RESULT => ${action} => ${reqData.uid} `, res.body, err);
		});
};

// Test #1 > GetWallet -> Richie (should be return credit 99999)
makeTestRequest('GetWallet', { uid: 'richie' });

// Test #2 > GetWallet -> anotherUser (should be return credit 0)
makeTestRequest('GetWallet', { uid: 'anotherUser' });

// Test #3 > GetWallet -> wrongUser (should be return error)
makeTestRequest('GetWallet', { uid: 'wrongUser' });

// Test #1 > UpdateWallet -> Richie -> buy card (should be return credit 777777)
makeTestRequest('UpdateWallet', {
	id: "0a148bf8-c96c-4e49-8b40-" + generateDummyNumber(),
	uid: 'richie',
	sid: generateDummyNumber(),
	action: 'buy',
	type: 'card',
	amount: 10
});


//API JWT EXAMPLE
var liveGamesApi = require('../index')(apiKey, apiSecret).API;
console.log('Test JWT =>', liveGamesApi.createJWT({id:1, name:'test user', parent:'LiveGamesResellerId'}));
