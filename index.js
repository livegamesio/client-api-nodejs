'use strict';

module.exports = function (apiKey, apiSecret) {
	return {
		API: require("./lib/api")(apiKey,apiSecret),
		Handler: require("./lib/handler")(apiSecret)
	};
};
