var jwt = require('jsonwebtoken');

module.exports = function(apiKey, apiSecret) {
	return {
		createJWT: function(userData, baseConfig) {

			var self = this,
				issued = Math.floor(Date.now() / 1000),
				expires = issued + 7200; //2h

			baseConfig = baseConfig || {};

			var tokenObject = self.xtend({
				game: "tombala",
				apiKey: apiKey,
				//sub: req.headers.origin,
				//aud: audience,
				exp: expires,
				//nbf: issued,
				iat: issued,
				jti: [apiKey, Date.now()].join('-'),
				user: userData
			}, baseConfig);

			return this.signToken(tokenObject);
		},
		signToken: function(payload) {
			return jwt.sign(payload, apiSecret);
		},
		xtend: function() {
			for (var i = 1; i < arguments.length; i++)
				for (var key in arguments[i])
					if (arguments[i].hasOwnProperty(key))
						arguments[0][key] = arguments[i][key];
			return arguments[0];
		}
	};
};
