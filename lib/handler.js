var crypto = require('crypto');

module.exports = function(apiSecret) {
	return {
		ACTIONS: {
			GetWallet: 'GetWallet',
			UpdateWallet: 'UpdateWallet',
			Ping: 'Ping'
		},
		createMiddleware: function(settings) {
			var self = this;
			if (!settings || !settings.bindings)
				throw new Error("INVALID_SETTINGS");

			return function(req, res, next) {
				if (settings.debug)
					console.log('[DEBUG] LiveGames Handling Request -> ', req.originalUrl, req.params, req.body);

				var action = self.getParamsFromRequest(req, 'lgAction'),
					data = self.getParamsFromRequest(req, 'lgData'),
					sec = self.getParamsFromRequest(req, 'lgSec'),
					prov = self.makeRequestSecret(data),
					cb = function(error, payload) {
						var rObj = !error ? {
							payload: payload,
							sec: self.makeRequestSecret(payload)
						} : {
							errors: Array.isArray && Array.isArray(error) ? error : [error]
						};

						if (settings.debug)
							console.log('[DEBUG] LiveGames Middleware Response -> ', rObj);

						res.json({
							response: rObj
						});
					};
				if (action && data && settings.bindings[action] && sec == prov) {
					settings.bindings[action](data, cb);
				} else {
					//console.log('InvalidRequest NEXT',action,data,req.originalUrl);
					next();
				}
			};

		},
		getParamsFromRequest: function(req, paramName) {
			if (req) {
				if (req.params && req.params[paramName])
					return req.params[paramName];

				if (req.body && req.body[paramName])
					return req.body[paramName];
			}
			return;
		},
		makeRequestSecret: function(data) {
			if (typeof data != "object" || !apiSecret) {
				console.log('ERROR: INVALID makeRequestSecret =>', data);
				return null;
			}
			var ds = [];
			try {
				Object.keys(data)
					.sort()
					.forEach(function(v, i) {
						if (typeof data[v] != "object")
							ds.push(data[v]);
					});
			} catch (exc) {
				console.log("[makeRequestSecret] Generating request security hash ERROR", exc);
			}
			ds.push(apiSecret);
			return crypto.createHash('md5').update(ds.join('.')).digest("hex");
		}
	};
};
